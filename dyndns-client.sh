#!/bin/bash

API_TOKEN=$1
DOMAIN=$2
NAME=$3

# Check if all the input variables have been set
#TODO

# Get the IP address
IP=$(curl --silent 'https://api.ipify.org?format=text')


# Get the id for the domain record
DOMAIN_RECORDS=$(curl --silent -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $API_TOKEN" "https://api.digitalocean.com/v2/domains/$DOMAIN/records" | jq -r ".domain_records")
DOMAIN_RECORD_COUNT=$(echo $DOMAIN_RECORDS | jq length)
DOMAIN_RECORD_ID=0

for (( c=0; c<$DOMAIN_RECORD_COUNT; c++ )); do
   if [ $(echo $DOMAIN_RECORDS | jq -r ".[$c] .name") = $NAME ]; then
      if [ $(echo $DOMAIN_RECORDS | jq -r ".[$c] .type") = "A" ]; then
         DOMAIN_RECORD_ID=$(echo $DOMAIN_RECORDS | jq -r ".[$c] .id")
      fi
   fi
done

# Check if the domain record id has been found
if [ $DOMAIN_RECORD_ID -eq 0 ]; then
   echo "No domain record id for \"$NAME.$DOMAIN\" found!"
   echo "Please check if the given domain is correct and if it has been created at Digital Ocean!"
   exit 1
fi

# Update the DNS record
UPDATE_RESPONSE=$(curl -i --silent -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $API_TOKEN" -d "{\"data\":\"$IP\"}" "https://api.digitalocean.com/v2/domains/$DOMAIN/records/$DOMAIN_RECORD_ID")

#echo $UPDATE_RESPONSE
